package com.example.misaki.myapp03;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import static android.graphics.Typeface.ITALIC;

public class LogInActivity extends AppCompatActivity {

    private Button buttonLogIn;
    private TextInputEditText editTextEmail;
    private TextInputEditText editTextPassword;
    private int ActivityNextCount = 0;
    private TextView textViewForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonLogIn = (Button)findViewById(R.id.buttonLogIn);
        buttonLogIn.setOnClickListener(buttonLogInListener);

        editTextEmail = (TextInputEditText)findViewById(R.id.editTextEmail);
        editTextPassword = (TextInputEditText)findViewById(R.id.editTextPassword);

        editTextPassword.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //if (actionId == EditorInfo.IME_ACTION_DONE) {
                //if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode()==KeyEvent.KEYCODE_ENTER) {
                    ActivityNext();
                    return true;
                //}
                //return false;
            }
        });

        textViewForgotPassword = (TextView) findViewById(R.id.textViewForgotPassword);
        textViewForgotPassword.setTypeface(null, ITALIC);
    }

    private OnClickListener buttonLogInListener = new OnClickListener() {
        public void onClick(View v) {
            ActivityNext();
        }
    };

    private void ActivityNext() {
        if(ActivityNextCount>0) {
            return;
        }
        ActivityNextCount++;
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Email", editTextEmail.getText().toString());
        intent.putExtra("Password", editTextPassword.getText().toString());
        startActivity(intent);
        finish();
    }

}
