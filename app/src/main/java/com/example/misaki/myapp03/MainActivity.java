package com.example.misaki.myapp03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textViewResultEmailValue;
    private TextView textViewRenamePasswordValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResultEmailValue = (TextView)findViewById(R.id.textViewResultEmailValue);
        textViewRenamePasswordValue = (TextView)findViewById(R.id.textViewResultPasswordValue);

        Intent intent = getIntent();
        String emailValue = intent.getStringExtra("Email");
        String passwordValue = intent.getStringExtra("Password");

        textViewResultEmailValue.setText(emailValue);
        textViewRenamePasswordValue.setText(passwordValue);
    }

}
